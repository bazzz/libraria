# librarian

Package librarian offers functions to organize media files on your computer such that the result may be interpreted by media centres like Kodi (and maybe others). It includes an executable as well to run librarian as a daemon.
