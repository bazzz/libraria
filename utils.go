package libraria

import (
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/bazzz/dates"
	"gitlab.com/bazzz/downloader"
	"gitlab.com/bazzz/file"
	"gitlab.com/bazzz/imvdb"
)

var (
	yearRegex       *regexp.Regexp
	fulldateRegex   *regexp.Regexp
	episodeRegex    *regexp.Regexp
	musicvideoRegex *regexp.Regexp
	cleanText       func(string) string = strings.NewReplacer(".", " ").Replace
)

func init() {
	r, err := regexp.Compile(`(19|20)\d{2}`)
	if err != nil {
		panic(err)
	}
	yearRegex = r

	r, err = regexp.Compile(`(20|19)\d{2}[^\d]+\d{2}[^\d]+\d{2}`)
	if err != nil {
		panic(err)
	}
	fulldateRegex = r

	r, err = regexp.Compile(`[S|s](\d+)[E|e](\d+)|(\d+)[×|x|X](\d+)`)
	if err != nil {
		panic(err)
	}
	episodeRegex = r

	r, err = regexp.Compile(` - `)
	if err != nil {
		panic(err)
	}
	musicvideoRegex = r
}

func value(input string, defaultValue string) string {
	if input == "" {
		return defaultValue
	}
	return input
}

func download(uri string, destination string) error {
	if uri == "" {
		return nil
	}
	return downloader.GetLazyHTTP(uri, destination)
}

func detectLanguage(filename string) (string, string) {
	filename = strings.TrimSuffix(filename, filepath.Ext(filename))
	tags := strings.Split(filename, " ")
	for _, tag := range tags {
		switch strings.ToLower(tag) {
		case "eng", "english":
			return "en", "English"
		case "ned", "dutch":
			return "nl", "Nederlands"
		case "ita", "italian":
			return "it", "Italiano"
		case "spanish":
			return "es", "Español"
		case "fra", "french":
			return "fr", "Française"
		case "german":
			return "de", "Deutsch"
		}
	}
	return "", ""
}

func getMaybeTitle(fileinfo string) string {
	test := strings.ToLower(fileinfo)
	tokens := []string{"2160p", "1080p", "720p", "hdtv", "webrip", "(medium)"}
	for _, token := range tokens {
		if pos := strings.Index(test, token); pos > 0 {
			return strings.Trim(fileinfo[:pos], " -")
		}
	}
	return ""
}

func getMaybePremiered(filename string) dates.Date {
	if pos := fulldateRegex.FindStringIndex(filename); pos != nil {
		premiered, _ := dates.Parse("2006-01-02", filename[pos[0]:pos[1]])
		return premiered
	}
	return dates.Date{}
}

func actorsFromIMVDB(input []imvdb.Artist) []Actor {
	result := make([]Actor, 0)
	for _, artist := range input {
		result = append(result, Actor{Name: artist.Name})
	}
	return result
}

func isSemanticEqual(input1 string, input2 string) bool {
	if input1 == input2 {
		return true
	}
	input1 = file.Sanitize(input1)
	input2 = file.Sanitize(input2)
	if strings.EqualFold(input1, input2) {
		return true
	}
	input1 = strings.ReplaceAll(input1, "&", "and")
	input2 = strings.ReplaceAll(input1, "&", "and")
	return strings.EqualFold(input1, input2)
}
