package libraria

import (
	"encoding/xml"

	"gitlab.com/bazzz/file"
	"gitlab.com/bazzz/imvdb"
)

func newMusicVideo(input *imvdb.MusicVideo) MusicVideo {
	m := MusicVideo{
		Title:  input.Title,
		Year:   input.Year,
		Artist: input.ArtistNames(),
		Actors: actorsFromIMVDB(input.Artists),
		Image:  input.Image(),
	}
	return m
}

// MusicVideo represents a movie.
type MusicVideo struct {
	XMLName xml.Name `xml:"musicvideo"`
	Title   string   `xml:"title"`
	Year    int      `xml:"year"`
	Artist  string   `xml:"artist"`
	Actors  []Actor  `xml:"actors"`
	Genres  []string `xml:"genre"`
	Image   string   `xml:"-"`
}

// Filename returns a string suitable to be used as a filename for this movie '<title> - <artist>'.
func (m MusicVideo) Filename() string {
	return file.Sanitize(m.Title + " - " + m.Artist)
}

// NFO returns this movie's nfo file xml represention.
func (m MusicVideo) NFO() ([]byte, error) {
	data, err := xml.MarshalIndent(m, "", "\t")
	result := append([]byte(xml.Header), data...)
	return result, err
}
